package net.qsoft.butterknife.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by QSPA10 on 2/17/2018.
 */

public class ApiClient {
    //public static final String BASE_URL = "http://103.43.93.162/sc/public/";
    public static final String BASE_URL = "http://shaoniiuc.com/";

    private static Retrofit retrofit = null;

    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}

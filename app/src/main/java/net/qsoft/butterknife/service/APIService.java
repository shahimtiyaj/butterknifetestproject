package net.qsoft.butterknife.service;

import net.qsoft.butterknife.Model.DataModel;
import net.qsoft.butterknife.Model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by QSPA10 on 2/17/2018.
 */

public interface APIService {
//    @GET("branch_information")
//    Call<List<DataModel>> getUserData();

    @GET("json_bangla")
    Call<List<User>> getUserData();
}

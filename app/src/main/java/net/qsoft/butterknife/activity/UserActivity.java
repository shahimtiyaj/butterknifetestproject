package net.qsoft.butterknife.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import net.qsoft.butterknife.Model.Data;
import net.qsoft.butterknife.Model.DataModel;
import net.qsoft.butterknife.Model.User;
import net.qsoft.butterknife.R;
import net.qsoft.butterknife.adapter.DataAdapter;
import net.qsoft.butterknife.adapter.RecyclerAdapter;
import net.qsoft.butterknife.adapter.UserAdapter;
import net.qsoft.butterknife.network.ApiClient;
import net.qsoft.butterknife.service.APIService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by QSPA10 on 2/17/2018.
 */

public class UserActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    UserAdapter userAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getUserList();
    }

    private void getUserList() {
        try {
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<List<User>> call = service.getUserData();

            call.enqueue(new Callback<List<User>>() {
                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                    Log.d("onResponse", response.message());
                    List<User> userList = response.body();
                    ButterKnife.bind(UserActivity.this);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                    recyclerView.setLayoutManager(mLayoutManager);
                    userAdapter = new UserAdapter(userList);
                    recyclerView.setAdapter(userAdapter);
                }

                @Override
                public void onFailure(Call<List<User>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Fail to connetct server!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
        }
    }
}

package net.qsoft.butterknife.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import net.qsoft.butterknife.Model.Data;
import net.qsoft.butterknife.Model.DataModel;
import net.qsoft.butterknife.R;
import net.qsoft.butterknife.adapter.DataAdapter;
import net.qsoft.butterknife.adapter.RecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private List<DataModel> dataList = new ArrayList<>();
    private List<Data> itemList = new ArrayList<>();

    RecyclerAdapter adapter;
    DataAdapter dataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Bind data-------------------------
        ButterKnife.bind(this);
        //Setup the RecyclerView------------
        dataAdapter = new DataAdapter(itemList);

        adapter = new RecyclerAdapter(dataList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        preparePoData();
    }

    private void preparePoData() {
        DataModel data = new DataModel("Food food: Butter butter",
                "Tast & Spicy", "208", "s", "d",
                "d", "d", "r", "t",
                "d", "d", "h", "y",
                "s", "s", "c");
        dataList.add(data);
    }

    private void prepareData() {

        Data data1 = new Data("Food food: Butter butter", "Tast & Spicy", "208");
        itemList.add(data1);

        Data data2 = new Data("Food food: Butter butter", "Tast & Spicy", "208");
        itemList.add(data2);

        Data data3 = new Data("Food food: Butter butter", "Tast & Spicy", "208");
        itemList.add(data3);

        Data data4 = new Data("Food food: Butter butter", "Tast & Spicy", "208");
        itemList.add(data4);

        adapter.notifyDataSetChanged();
    }


}

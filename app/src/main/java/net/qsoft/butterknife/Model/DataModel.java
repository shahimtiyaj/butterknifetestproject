package net.qsoft.butterknife.Model;

/**
 * Created by Md. Imtiyaj on 30-Dec-17.
 */

public class DataModel {

    String cono;
    String coname;
    String sessionno;
    String opendate;
    String openingbal;
    String password;
    String emthod;
    String cashinhand;
    String enterdby;
    String deviceid;
    String status;
    String branchcode;
    String branchname;
    String projectcode;
    String desig;
    String lastposynctime;


    public String getDesig() {
        return desig;
    }

    public void setDesig(String desig) {
        this.desig = desig;
    }

    public String getLastposynctime() {
        return lastposynctime;
    }

    public void setLastposynctime(String lastposynctime) {
        this.lastposynctime = lastposynctime;
    }


    public DataModel() {
        super();
    }

    public DataModel(String cono, String coname, String sessionno, String opendate,
                     String openingbal, String password, String emthod, String cashinhand,
                     String enterdby, String deviceid, String status, String branchcode,
                     String branchname, String projectcode, String desig, String lastposynctime) {

        this.cono = cono;
        this.coname = coname;
        this.sessionno = sessionno;
        this.opendate = opendate;
        this.openingbal = openingbal;
        this.password = password;
        this.emthod = emthod;
        this.cashinhand = cashinhand;
        this.enterdby = enterdby;
        this.deviceid = deviceid;
        this.status = status;
        this.branchcode = branchcode;
        this.branchname = branchname;
        this.projectcode = projectcode;
        this.desig = desig;
        this.lastposynctime = lastposynctime;
    }

    public String getCono() {
        return cono;
    }

    public void setCono(String cono) {
        this.cono = cono;
    }

    public String getConame() {
        return coname;
    }

    public void setConame(String coname) {
        this.coname = coname;
    }

    public String getSessionno() {
        return sessionno;
    }

    public void setSessionno(String sessionno) {
        this.sessionno = sessionno;
    }

    public String getOpendate() {
        return opendate;
    }

    public void setOpendate(String opendate) {
        this.opendate = opendate;
    }

    public String getOpeningbal() {
        return openingbal;
    }

    public void setOpeningbal(String openingbal) {
        this.openingbal = openingbal;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmthod() {
        return emthod;
    }

    public void setEmthod(String emthod) {
        this.emthod = emthod;
    }

    public String getCashinhand() {
        return cashinhand;
    }

    public void setCashinhand(String cashinhand) {
        this.cashinhand = cashinhand;
    }

    public String getEnterdby() {
        return enterdby;
    }

    public void setEnterdby(String enterdby) {
        this.enterdby = enterdby;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBranchcode() {
        return branchcode;
    }

    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getProjectcode() {
        return projectcode;
    }

    public void setProjectcode(String projectcode) {
        this.projectcode = projectcode;
    }
}

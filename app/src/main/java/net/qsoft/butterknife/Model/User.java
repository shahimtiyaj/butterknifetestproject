package net.qsoft.butterknife.Model;

/**
 * Created by QSPA10 on 2/17/2018.
 */

public class User {
    private String name;
    private String hobby;


    public User() {
    }


    public User(String name, String hobby) {
        super();
        this.name = name;
        this.hobby = hobby;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }
}

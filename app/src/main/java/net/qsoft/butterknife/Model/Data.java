package net.qsoft.butterknife.Model;

/**
 * Created by QSPA10 on 2/15/2018.
 */

public class Data {
    private String title, name, year;

    public Data() {
    }

    public Data(String title, String name, String year) {
        this.title = title;
        this.name = name;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package net.qsoft.butterknife.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qsoft.butterknife.R;
import net.qsoft.butterknife.Model.DataModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by QSPA10 on 2/17/2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    private List<DataModel> itemList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cono)
        TextView cono;

        @BindView(R.id.coname)
        TextView coname;

        @BindView(R.id.sessionno)
        TextView sessionno;

        @BindView(R.id.opendate)
        TextView opendate;

        @BindView(R.id.openingbal)
        TextView openingbal;

        @BindView(R.id.password)
        TextView password;

        @BindView(R.id.emethod)
        TextView emethod;

        @BindView(R.id.cashinhand)
        TextView cashinhand;

        @BindView(R.id.enteredby)
        TextView enteredby;

        @BindView(R.id.deviceid)
        TextView deviceid;

        @BindView(R.id.status)
        TextView status;

        @BindView(R.id.branchcode)
        TextView branchcode;

        @BindView(R.id.branchname)
        TextView branchname;

        @BindView(R.id.projectcode)
        TextView projectcode;

        @BindView(R.id.desig)
        TextView desig;

        @BindView(R.id.lastposynctime)
        TextView lastposynctime;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public RecyclerAdapter(List<DataModel> itemList) {
        this.itemList = itemList;
    }

    @Override
    public RecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_polist, parent, false);

        return new RecyclerAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.MyViewHolder holder, int position) {

        DataModel dataModel = itemList.get(position);

        holder.cono.setText(dataModel.getCono());
        holder.coname.setText(dataModel.getConame());
        holder.sessionno.setText(dataModel.getSessionno());
        holder.opendate.setText(dataModel.getOpendate());
        holder.openingbal.setText(dataModel.getOpeningbal());
        holder.password.setText(dataModel.getPassword());
        holder.emethod.setText(dataModel.getEmthod());
        holder.cashinhand.setText(dataModel.getCashinhand());
        holder.enteredby.setText(dataModel.getEnterdby());
        holder.deviceid.setText(dataModel.getDeviceid());
        holder.status.setText(dataModel.getStatus());
        holder.branchcode.setText(dataModel.getBranchcode());
        holder.branchname.setText(dataModel.getBranchname());
        holder.projectcode.setText(dataModel.getProjectcode());
        holder.desig.setText(dataModel.getDesig());
        holder.lastposynctime.setText(dataModel.getLastposynctime());

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

}
